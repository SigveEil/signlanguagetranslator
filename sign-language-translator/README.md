--Login--
Login a user with username and sets the state for them

--Navbar--
Navbar to access profile and translations

--Translate--
User can type in letters and have them translated to sign language

--Profile--
Display user profile, previous translate searches would pop up here

--Styling--
This application uses simple bootstrap styling