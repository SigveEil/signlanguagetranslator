import { handleFirstRespone } from "../../utils/apiUtils";

export const TranslateAPI = {
  translation(letter) {
    //Attempts to check the input in the database to get a matching image
    return fetch(`http://localhost:8000/images/${letter}`, {
      headers: {
        "Content-Type": "application/json",
      },
      //Handles errors
    }).then(handleFirstRespone);
  },
};
