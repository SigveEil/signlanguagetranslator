import AppContainer from "../../hoc/AppContainer";
import { translateAttemptAction } from "../../store/actions/translateAction";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { TranslateAPI } from "./TranslateAPI";
import styles from "./Translate.module.css";

const Translate = () => {
  //Preparing for the translation
  const dispatch = useDispatch();
  const { fetchingError } = useSelector((state) => state.translateReducer);

  const [letter, setLetter] = useState({
    translateThis: "",
  });

  const [state, setState] = useState({
    fetchingTranslation: true,
    translation: [],
    error: "",
  });

  //Setting the input field
  const onInputChange = (event) => {
    setLetter({
      ...letter,
      [event.target.id]: event.target.value.trim(),
    });
  };

  //Commits on translate click to fetch letter url
  const onFormSubmit = (event) => {
    event.preventDefault(); // stop the page reload
    dispatch(translateAttemptAction(letter.translateThis));
    TranslateAPI.translation(letter.translateThis)
      .then((translation) => {
        setState({
          ...state,
          fetchingTranslation: false,
          translation: [translation],
        });
        console.log(translation);
      })
      //handles error
      .catch((error) => {
        setState({
          ...state,
          fetchingTranslation: false,
          error: error.message,
        });
      });
  };

  return (
    <AppContainer>
      <form className="mt-3 mb-4" onSubmit={onFormSubmit}>
        {/* HORRIBLE way of adding space, but navbar keeps blocking it... */}
        <br />
        <br />
        <br />
        <h1>Translation Station</h1>
        <p>Welcome to the translate page!</p>
        <div className="mb-3">
          <label htmlFor="translateThis" className="form-label">
            Translate:
          </label>
          <input
            id="translateThis"
            type="text"
            placeholder="Start translating!"
            className="form-control"
            onChange={onInputChange}
          />
        </div>
        {/* Take the input from above into TranslateAPI, show it under here */}
        <button type="submit" className="btn btn-primary btn-lg">
          Translate
        </button>
      </form>

      <div className="mt-5">
        <h4>Translation:</h4>
        <p>
          The translator can at the moment only translate one letter at the
          time, sorry for the incovenience!
        </p>
        <div className={styles.Translations}>
          {state.translation.map((translation) => (
            <div key={translation.id}>
              <img
                src={translation.url}
                alt={translation.id}
                className={styles.TranslationImage}
              />
            </div>
          ))}
        </div>
      </div>

      {/* Shows an error to the user if there was something wrong with the fetching */}
      {fetchingError && (
        <div className="alert alert-danger" role="alert">
          <h4>Translation Failed</h4>
          <p>{fetchingError}</p>
        </div>
      )}
    </AppContainer>
  );
};
export default Translate;
