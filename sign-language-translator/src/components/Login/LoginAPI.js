import { handleFirstRespone } from "../../utils/apiUtils";

export const LoginAPI = {
  login(credentials) {
    return fetch(
      //Stores the user in the json database and creates an ID for them
      "http://localhost:8000/users",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(credentials),
      }
      //Handles error messages
    ).then(handleFirstRespone);
  },
};
