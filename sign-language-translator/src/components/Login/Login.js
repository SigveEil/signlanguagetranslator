import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import { loginAttemptAction } from "../../store/actions/loginAction";

const Login = () => {
  //Setting states, loggedIn and preparing credentials
  const dispatch = useDispatch();
  const { loginAttempting } = useSelector((state) => state.loginReducer);
  const { loggedIn } = useSelector((state) => state.sessionReducer);

  const [credentials, setCredentials] = useState({
    username: "",
    id: "",
  });
  //Updates the localstorage with every new letter typed
  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      [event.target.id]: event.target.value,
    });
  };
  //User gets logged in with credential(s)
  const onFormSubmit = (event) => {
    event.preventDefault(); // stop the page reload
    dispatch(loginAttemptAction(credentials));
  };

  return (
    <>
      {/* User gets redirected away from the login screen if they are logged in */}
      {loggedIn && <Redirect to="/translate" />}
      {!loggedIn && (
        <AppContainer>
          {/* HORRIBLE way of adding space, but navbar keeps blocking it... */}
          <br />
          <br />
          <br />
          <form className="mt-3 mb-4" onSubmit={onFormSubmit}>
            <h1>Welcome to the Sign Language Translator!</h1>
            <p>Enter your name to get started</p>

            <div className="mb-3">
              <label htmlFor="username" className="form-label">
                Username
              </label>
              <input
                id="username"
                type="text"
                placeholder="What's your name?"
                className="form-control"
                onChange={onInputChange}
              />
            </div>

            <button type="submit" className="btn btn-primary btn-lg">
              Start Translating!
            </button>
          </form>

          {loginAttempting && <p>Trying to login...</p>}
        </AppContainer>
      )}
    </>
  );
};

export default Login;
