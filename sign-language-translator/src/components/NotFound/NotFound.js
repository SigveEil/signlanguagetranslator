//In case the user tries to access a page that does not exist
//They can be redirected to the start page
import { Link } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";

const NotFound = () => {
  return (
    <AppContainer>
      <main>
        <h4>Stop being sneaky 👀.</h4>
        <p>This page does not exist</p>
        <Link to="/">Take me home</Link>
      </main>
    </AppContainer>
  );
};
export default NotFound;
