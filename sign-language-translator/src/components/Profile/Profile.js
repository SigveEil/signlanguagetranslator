import AppContainer from "../../hoc/AppContainer";

const Profile = () => {
  return (
    <AppContainer>
      {/* HORRIBLE way of adding space, but navbar keeps blocking it... */}
      <br />
      <br />
      <br />
      {/* Add functionality */}
      <h1>Hey! This is you!</h1>
      <p>These are your previous searches!</p>
    </AppContainer>
  );
};
export default Profile;
