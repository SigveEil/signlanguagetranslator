import { Link, NavLink } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import "bootstrap/dist/js/bootstrap.bundle";

export const Navbar = () => {
  return (
    //Navbar with buttons
    <nav className="navbar navbar-expand-md navbar-light bg-light mb-3 fixed-top">
      <AppContainer>
        <Link className="navbar-brand me-auto mb-2 mb-lg-0" to="/translate">
          Translation Station
        </Link>
        <div className="collapse navbar-collapse" id="mainNavbar">
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink className="nav-link d-flex" to="/profile">
                <p>Profile</p>
                <span className="material-icons">account_circle</span>
              </NavLink>
            </li>
          </ul>
        </div>
      </AppContainer>
    </nav>
  );
};
