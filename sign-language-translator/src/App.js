import "./App.css";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import NotFound from "./components/NotFound/NotFound";
import Login from "./components/Login/Login";
import Translate from "./components/Translate/Translate";
import {Navbar} from "./components/Navbar/Navbar";
import Profile from "./components/Profile/Profile";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        {/* Sets up the url with pages */}
        <Switch>
          <Route path="/" exact component={Login} />
          <Route path="/translate" component={Translate} />
          <Route path="/profile" component={Profile} />
          <Route path="*" component={NotFound} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
