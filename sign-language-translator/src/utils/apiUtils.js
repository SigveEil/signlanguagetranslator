//Handles errors for APIs
export const handleFirstRespone = async (response) => {
  if (!response.ok) {
    const { error = "Unkown error occured." } = await response.json();
    throw new Error(error);
  }
  return response.json();
};