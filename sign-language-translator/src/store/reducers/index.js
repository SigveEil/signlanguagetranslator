import { combineReducers } from "redux";
import { loginReducer } from "./loginReducer";
import { sessionReducer } from "./sessionReducers";
import { translateReducer } from "./translateReducer";

const appReducer = combineReducers({
  loginReducer,
  sessionReducer,
  translateReducer
});

export default appReducer;
