import {
  ACTION_TRANSLATE_FETCHING,
  ACTION_TRANSLATE_ERROR,
  ACTION_TRANSLATE_SUCCESS,
  ACTION_TRANSLATE_ATTEMPTING,
} from "../actions/translateAction";

const initialState = {
  translateFetching: false,
  fetchingError: "",
};

export const translateReducer = (state = { ...initialState }, action) => {
  switch (action.type) {
    case ACTION_TRANSLATE_ATTEMPTING:
      return{
        ...state,
        translateAttempting: true,
        fetchingError: ""
  }
    case ACTION_TRANSLATE_FETCHING:
      return {
        ...state,
        translateFetching: true,
        fetchingError: "",
      };
    case ACTION_TRANSLATE_ERROR:
      return {
        ...state,
        translateFetching: false,
        fetchingError: action.payload,
      };
    case ACTION_TRANSLATE_SUCCESS:
      return {
        ...state,
        translateFetching: false,
        fetchingError: "",
      };
    default:
      return state;
  }
};
