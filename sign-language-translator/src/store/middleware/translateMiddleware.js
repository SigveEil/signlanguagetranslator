import { TranslateAPI } from "../../components/Translate/TranslateAPI";
import {
  ACTION_TRANSLATE_ATTEMPTING,
  translateErrorAction,
  translateSuccessAction,
} from "../actions/translateAction";

export const translateMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_TRANSLATE_ATTEMPTING) {
      TranslateAPI.translation(action.payload)
        .then((word) => {
          dispatch(translateSuccessAction(word));
        })
        .catch((error) => {
          dispatch(translateErrorAction(error.message));
        });
    }
  };
