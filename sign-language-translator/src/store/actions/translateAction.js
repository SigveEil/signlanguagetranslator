export const ACTION_TRANSLATE_ATTEMPTING = "[translate] ATTEMPTING"
export const ACTION_TRANSLATE_FETCHING = "[translate] FETCHING";
export const ACTION_TRANSLATE_ERROR = "[translate] ERROR";
export const ACTION_TRANSLATE_SUCCESS = "[translate] SUCCESS";

export const translateAttemptAction = letter => ({
  type: ACTION_TRANSLATE_ATTEMPTING,
  payload: letter
})

export const translateFetchingAction = (letter) => ({
  type: ACTION_TRANSLATE_FETCHING,
  payload: letter,
});

export const translateErrorAction = (error) => ({
  type: ACTION_TRANSLATE_ERROR,
  payload: error,
});

export const translateSuccessAction = letter => ({
  type: ACTION_TRANSLATE_SUCCESS,
  payload: letter,
});
