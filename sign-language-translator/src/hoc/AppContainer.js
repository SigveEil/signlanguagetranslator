//Move away from the edge of screen
const AppContainer = ({ children }) => {
  return <div className="container">{children}</div>;
};
export default AppContainer;
